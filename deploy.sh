ENV=$1
case ${ENV} in
  "DEV")
    CLUSTER=${DEV_CLUSTER}
    REGION=${DEV_REGION}
    ;;
  "QA")
    CLUSTER=${QA_CLUSTER}
    REGION=${QA_REGION}
    ;;
  "UAT")
    CLUSTER=${UAT_CLUSTER}
    REGION=${UAT_REGION}
    ;;
  "PROD")
    CLUSTER=${PROD_CLUSTER}
    REGION=${PROD_REGION}
    ;;
  *)
    echo "Usage: deploy.sh {DEV,QA,UAT,PROD}"
    exit 1
    ;;
esac

IMAGE_TAG="${IMAGE_REPO}:$( echo ${CI_COMMIT_SHA} | cut -c -8 )"

echo ${CLUSTER}
echo ${REGION}
echo ${IMAGE_TAG}

# mkdir gcp_config/ .kube/
# echo ${GCP_AUTH_KEYFILE} > gcp_config/keyfile.json
# docker run -v $(pwd)/gcp_config:/root/.config google/cloud-sdk:latest gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
# docker run -v $(pwd)/gcp_config:/root/.config -v $(pwd)/.kube:/root/.kube google/cloud-sdk:latest gcloud beta container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${GCP_PROJECT}
# docker run -v $(pwd)/gcp_config:/root/.config -v $(pwd)/.kube:/root/.kube -v $(pwd)/k8s_manifests:/root/k8s google/cloud-sdk:latest kubectl apply -f /root/k8s/webapp-deploy.yaml
# docker run -v $(pwd)/gcp_config:/root/.config -v $(pwd)/.kube:/root/.kube -v $(pwd)/k8s_manifests:/root/k8s google/cloud-sdk:latest kubectl apply -f /root/k8s/webapp-service.yaml
# docker run -v $(pwd)/gcp_config:/root/.config -v $(pwd)/.kube:/root/.kube -v $(pwd)/k8s_manifests:/root/k8s google/cloud-sdk:latest kubectl apply -f /root/k8s/webapp-ingress.yaml
