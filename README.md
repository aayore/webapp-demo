### Run this webapp

1. `pip install -r requirements.txt`
1. `cd mysite`
1. `python manage.py runserver`
1. [Check it out](http://localhost:8000/polls)


### Create an admin

1. `python manage.py createsuperuser`
1. Enter a username (e.g. `admin` or `siteadmin`)
1. Enter an e-mail
1. Enter a password (twice)
1. [Do some adminning!](http://localhost:8000/admin)
