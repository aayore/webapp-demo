FROM python:3.6

# This can be anything, but must match the port in the CMD
EXPOSE 8000

# This makes sure that Python output will be shown immediately
ENV PYTHONUNBUFFERED 1

WORKDIR /code/mysite

# We're explicit here; don't add Docker or K8s components
ADD requirements.txt /code
ADD mysite/ /code/mysite/

RUN pip install --upgrade pip && \
    pip install -r /code/requirements.txt

CMD [ "python3", "manage.py", "runserver", "0.0.0.0:8000" ]
